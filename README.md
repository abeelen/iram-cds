# IRAM @ CDS


Scripts to vizualize the IRAM observing database hosted at the CDS.

The main goal of those scripts was to be able to draw some statistics on the sources observed at the IRAM observatories. Turns out we could also display them nicely.

## PdBI
```
Latest entry : 2014-04-01 01:41:00.000
Sources Statistics
------------------
M82 :	 522 hrs
IRC+10216 :	 393 hrs
J1148+5251 :	 353 hrs
APM08279 :	 338 hrs
IC342 :	 274 hrs
L1157 :	 269 hrs
MRK231 :	 267 hrs
NGC7023 :	 205 hrs
10214+4724 :	 204 hrs
POLARIS :	 191 hrs
B0218 :	 185 hrs
NGC1068 :	 182 hrs
ARP220 :	 179 hrs
NGC7027 :	 177 hrs
W3OH :	 169 hrs
1830-211 :	 163 hrs
GG TAU :	 162 hrs
HDFN :	 162 hrs
CRL618 :	 159 hrs
```

![Sources observed at the PdBI](pdbi.gif)

## 30m
```
Sources Statistics
------------------
Mars :	 331 hrs
horsehead :	 327 hrs
M33 :	 301 hrs
L1544 :	 253 hrs
Uranus :	 213 hrs
IRC+10216 :	 198 hrs
W43 :	 181 hrs
W3OH :	 172 hrs
L1157-B1 :	 166 hrs
M51 :	 131 hrs
NGC891 :	 122 hrs
M82 :	 122 hrs
CEPE :	 119 hrs
M33-South :	 117 hrs
NGC1333 :	 117 hrs
L1527 :	 114 hrs
NGC1068 :	 109 hrs
OriBarDF :	 107 hrs
0316+413 :	 103 hrs
```


![Sources observed at the 30m](30m.gif)
