from os import path
import numpy as np
import datetime

from matplotlib import pyplot as plt
from matplotlib import colors
from matplotlib.animation import FuncAnimation

from astropy import units as u
from astropy.time import Time
from astropy import table
from astropy.io import votable


def get_table(name='pdbi', download=False):
    """
    Read or download the votable from the CDS and
    construct the needed catalogs

    Parameters
    ----------
    name : str
        the name of the observatory ( pdbi | 30m )
    download : bool
        if True, force the download from CDS

    Returns
    -------
    cat_obs, cat_pi
        the 2 catalogs from CDS
    """

    if '+' in name:
        names = name.split('+')

        cats = []

        for name in names:
            cats.append(get_table(name, download=download))

        for col in ['date-start', 'date-end', 'date-obs']:
            for cat in cats:
                cat[col] = cat[col].to_datetime()

        cat = table.vstack(cats)

        for col in ['date-start', 'date-end', 'date-obs']:
            cat[col] = Time(cat[col])

        cat.meta['description'] = name
        return cat


    # Check if files are present and download if needed
    if (not path.isfile(name+'.votable')) or \
       (not path.isfile(name+'_pi.votable')) or \
       download:

        from astroquery.vizier import Vizier
        v = Vizier(catalog='B/iram/{}'.format(name),
                   columns=['*', '_RAJ2000', '_DEJ2000', 'Prog', 'Obs', 'tos', 't1', 't2'])
        v.ROW_LIMIT = -1
        cats = v.get_catalogs('B/iram/{}*'.format(name))
        cat_obs = cats['B/iram/{}'.format(name)]
        cat_pi = cats['B/iram/{}_pi'.format(name)]
        cat_obs.write(name+'.votable', format='votable', overwrite=True)
        cat_pi.write(name+'_pi.votable', format='votable', overwrite=True)

    cat = format_table(name)

    return cat


def format_table(name):

    if (not path.isfile('{}.fits'.format(name))) or \
       (path.getmtime('{}.fits'.format(name)) < path.getmtime('{}.votable'.format(name))) or \
       (path.getmtime('{}.fits'.format(name)) < path.getmtime('{}_pi.votable'.format(name))):

        cat_obs = votable.parse(
            name+'.votable', unit_format="cds").get_first_table().to_table()
        cat_pi = votable.parse(
            name+'_pi.votable', unit_format="cds").get_first_table().to_table()

        cat_obs.convert_bytestring_to_unicode()
        cat_pi.convert_bytestring_to_unicode()

        for item in ['t1', 't2']:
            if np.any(cat_obs[item] == ''):
                bad = cat_obs[item] == ''
                cat_obs[item].mask[bad] = True
            cat_obs[item].fill_value = '00:00:00'

        # Remove un-standard units
        for item in ['t1', 't2', 'RAJ2000', 'DEJ2000', 'Obs']:
            cat_obs[item].unit = None

        # Modify some of the columns

        cat_obs.add_column(table.MaskedColumn(
            name='ra', data=cat_obs['_RAJ2000']))
        cat_obs.add_column(table.MaskedColumn(
            name='dec', data=cat_obs['_DEJ2000']))

        date_start = np.array([Time(date+'T'+hour).mjd for
                               date, hour in zip(cat_obs['Obs'],
                                                 cat_obs['t1'].filled())])
        date_end = np.array([Time(date+'T'+hour).mjd for
                             date, hour in zip(cat_obs['Obs'],
                                               cat_obs['t2'].filled())])
        # Midnigth trick
        date_end[(date_end-date_start) < 0] += 1

        cat_obs['date-obs'] = Time(cat_obs['Obs'])

        cat_obs['date-start'] = Time(date_start, format='mjd')
        cat_obs['date-end'] = Time(date_end, format='mjd')

        if 'tos' not in cat_obs.dtype.names:
            tos = (date_end-date_start)*u.d
            cat_obs['tos'] = tos
            cat_obs['tos'].description='rough estimate of time on source'

        cat_obs['tos'] = cat_obs['tos'].to(u.h)

        cat = table.join(cat_obs, cat_pi, keys='Prog')
        cat.meta = cat_obs.meta
        cat.write('{}.fits'.format(name), overwrite=True)

    else:
        cat = Table.read('{}.fits'.format(
            name), format='fits', astropy_native=True)
        cat.meta['description'] = name

    return cat


def plot_overhead(cat):

    tos = cat['tos']
    too = (cat['date-end']-cat['date-start'])*24
    overhead = too / tos

    plt.figure()
    plt.title('Overhead of ' + cat.meta['description'])
    plt.scatter(tos, overhead,
                c=Time(cat['date-start'].data,
                       format='mjd').jyear.astype(np.int),
                linewidth=0, alpha=0.5)
    plt.yscale('log')
    plt.hlines(1, tos.min(), tos.max(), linestyle='dotted')
    plt.hlines(1.6, tos.min(), tos.max(), linestyle='dashed', colors='red')

    plt.xlim(tos.min(), tos.max())
    plt.ylim(overhead[overhead != 0].min(), overhead.max())
    plt.xlabel('time on source [h]')
    plt.ylabel('time on observation / time on source')
    cbar = plt.colorbar()
    cbar.set_label('DATE-START year')


def animate_obs(cat, n_frames=25*10, time_trans=60):

    tos = cat['tos']
    ra = cat['ra']
    dec = cat['dec']
    date_start = cat['date-obs']

    size_dots = (tos-tos.min())/(tos.max()-tos.min())*500+20

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title(cat.meta['description'])
    dots = ax.scatter(ra, dec, s=size_dots*0, c=tos, norm=colors.LogNorm(),
                      linewidth=0, alpha=0.5)
    date_text = ax.text(0, -50, '', verticalalignment='bottom')

    ax.set_xlabel('RA [deg]')
    ax.set_ylabel('DEC [deg]')
    ax.grid()

    cbar = fig.colorbar(dots)
    cbar.set_label('time on obs [h]')

    def update(frame, n_frames, date_start, time_trans, size_dots):
        date_frame = (date_start.max()-date_start.min()) * \
            frame/n_frames+date_start.min()
        dots.set_sizes(
            (1-(np.tanh((date_start-date_frame)/time_trans)+1)/2) * size_dots)
        date_text.set_text(Time(date_frame, format='mjd').jyear_str)

    return FuncAnimation(fig, update, fargs=(n_frames, date_start, time_trans, size_dots), frames=n_frames, repeat=False)


def plot_sources(cat):

    name_sources = cat['Name']

    unique_source = np.unique(name_sources)

    time_per_source = np.array([cat['tos'][name_sources == source].sum() for
                                source in unique_source])
    ra_per_source = np.array([cat['ra'][name_sources == source].mean() for
                              source in unique_source])
    dec_per_source = np.array([cat['dec'][name_sources == source].mean() for
                               source in unique_source])

    plt.figure()
    plt.title(cat.meta['description'])
    n, bins, patches = plt.hist(time_per_source, bins=100)
    plt.yscale('log')
    plt.ylim(8e-1, n.max())
    plt.xlabel('time per source [h]')
    plt.ylabel('number of sources')

    plt.figure()
    plt.title(cat.meta['description'])

    size_dots = (time_per_source-time_per_source.min()) / \
        (time_per_source.max()-time_per_source.min())*500+20

    plt.scatter(ra_per_source, dec_per_source, s=size_dots,
                c=time_per_source, norm=colors.LogNorm(),
                linewidth=0, alpha=0.5)
    plt.xlabel('RA [deg]')
    plt.ylabel('DEC [deg]')
    plt.grid()
    cbar = plt.colorbar()
    cbar.set_label('time per source [h]')

    print('Sources Statistics')
    print('------------------')
    for index in np.argsort(time_per_source)[:-20:-1]:
        print('%s :\t %i hrs' % (unique_source[index], time_per_source[index]))


def plot_PI(cat):

    for col in ['date-start', 'date-end', 'date-obs']:
        cat[col] = cat[col].to_datetime()

    df = cat[['date-obs', 'PI', 'tos']].to_pandas()
    df = df.set_index('date-obs')

    df = df.pivot_table(index=df.index, columns='PI',
                        values='tos', fill_value=0).resample('Y').sum()

    n_print = 20
    n_plot = 15
    n_year = 10

    total_time = df.sum().sort_values(ascending=False)
    df_per_cent = (df.T / df.sum(axis=1)).T

    sub_df = df[df.index > df.index.max() - datetime.timedelta(n_year*365)]
    sub_time = sub_df.sum().sort_values(ascending=False)
    sub_df_per_cent = (sub_df.T / sub_df.sum(axis=1)).T

    print('Overal PI Statistics')
    print('-------------')
    print(total_time[0:n_print])

    print('Last {}yrs PI Statistics'.format(n_year))
    print('-------------')
    print(sub_time[0:n_print])

    fig, axes = plt.subplots(nrows=2, ncols=2)
    fig.suptitle(cat.meta['description'])

    axes[0, 0].set_title('{} highest over the full database'.format(n_plot))
    axes[0, 0].set_ylabel('# of hours')
    axes[0, 1].set_title('{} highest over the last {} years'.format(n_plot, n_year))

    df[total_time.index[0:n_plot]].plot(kind='area', ax=axes[0, 0])
    df[total_time.index[0:n_plot]].cumsum().plot(
        kind='area', ax=axes[1, 0], legend=False)

    sub_df[sub_time.index[0:n_plot]].plot(kind='area', ax=axes[0, 1])
    sub_df[sub_time.index[0:n_plot]].cumsum().plot(
        kind='area', ax=axes[1, 1], legend=False)

    fig, axes = plt.subplots(ncols=2)
    fig.suptitle(cat.meta['description'])

    axes[0].set_title('{} highest over the full database'.format(n_plot))
    axes[0].set_ylabel('% of observed time')
    axes[1].set_title('{} highest over the last {} years'.format(n_plot, n_year))

    df_per_cent[total_time.index[0:n_plot]].plot(kind='area', ax=axes[0])
    sub_df_per_cent[sub_time.index[0:n_plot]].plot(kind='area', ax=axes[1])

    # unique_pi = np.unique(cat['PI'])
    # total_time_per_pi = np.zeros(len(unique_pi))

    # for iPI, pi in enumerate(unique_pi):
    #     total_time_per_pi[iPI] += cat['tos'][ pi == cat['PI']].filled(0).sum()

    # # 1 points a year
    # min_date = np.min(cat['date-obs'])
    # max_date = np.max(cat['date-obs'])
    # # total_time = np.linspace(min_date,max_date,
    # #                          np.int((max_date-min_date)*2) )
    # total_time = np.linspace(min_date, max_date,
    #                          np.int((max_date-min_date)) )

    # time_per_pi = np.zeros((len(unique_pi), len(total_time)), dtype=np.float)

    # for iPI, pi in enumerate(unique_pi):
    #     for iTime, time in enumerate(total_time[1:]):
    #         this_time_this_pi = np.bitwise_and(cat['PI'] == pi,
    #                                            np.bitwise_and(cat['date-obs'] > total_time[iTime], cat['date-obs'] < time) )
    #         time_per_pi[iPI, iTime] += cat['tos'][this_time_this_pi].filled(0).sum()

    # plt.figure()
    # plt.title(cat.meta['description'])
    # n, bins, patches = plt.hist(total_time_per_pi,bins=100)
    # plt.yscale('log')
    # plt.ylim(8e-1, n.max())
    # plt.xlabel('time per PI [h]')
    # plt.ylabel('number of PI')

    # print('Overal PI Statistics')
    # print('-------------')
    # biggest_time = np.argsort(time_per_pi.sum(axis=1))[-1:-25:-1]

    # for index in biggest_time:
    #     print('%s :\t %i hrs'%(unique_pi[index], total_time_per_pi[index]))

    # fig,axes = plt.subplots(2,1, sharex=True)
    # axes[0].set_title('Overal PI Statistics')
    # axes[0].stackplot(total_time[:-1], time_per_pi[biggest_time,:-1], labels=unique_pi[biggest_time].data,colors=plt.cm.gist_ncar(np.linspace(0,1,len(biggest_time))).tolist())
    # axes[0].set_ylabel('observed time')
    # axes[1].stackplot(total_time[:-1], np.cumsum(time_per_pi,axis=1)[biggest_time,:-1], labels=unique_pi[biggest_time].data,colors=plt.cm.gist_ncar(np.linspace(0,1,len(biggest_time))).tolist())
    # axes[1].set_ylabel('cumulative observed time')
    # axes[1].set_xlabel('years')
    # axes[1].legend(loc='upper left', frameon=False, fontsize='xx-small', ncol=2)

    # n_years = 5
    # print('Last 5yrs PI Statistics')
    # print('-------------')
    # biggest_time = np.argsort(time_per_pi[:,-n_years:].sum(axis=1))[-1:-25:-1]

    # for index in biggest_time:
    #     print('%s :\t %i hrs'%(unique_pi[index], total_time_per_pi[index]))

    # fig,axes = plt.subplots(2,1, sharex=True)
    # axes[0].set_title('Last 5yrs PI Statistics')
    # axes[0].stackplot(total_time[-n_years:-1], time_per_pi[biggest_time,-n_years:-1], labels=unique_pi[biggest_time].data,colors=plt.cm.gist_ncar(np.linspace(0,1,len(biggest_time))).tolist())
    # axes[0].set_ylabel('observed time')
    # axes[1].stackplot(total_time[-n_years:-1], np.cumsum(time_per_pi,axis=1)[biggest_time,-n_years:-1], labels=unique_pi[biggest_time].data,colors=plt.cm.gist_ncar(np.linspace(0,1,len(biggest_time))).tolist())
    # axes[1].set_ylabel('cumulative observed time')
    # axes[1].set_xlabel('years')
    # axes[1].legend(loc='upper left', frameon=False, fontsize='xx-small', ncol=2)


def test_healpy(cat):
    import healpy as hp

    # https://github.com/lsst/sims_selfcal/blob/master/python/lsst/sims/selfcal/analysis/healplots.py
    def healbin(ra, dec, values, nside=128, reduceFunc=np.mean, dtype='float', returnPixAng=False):
        """Take arrays of ra, dec, and value and bin into healpixels  """

        lat = np.pi/2. - dec
        hpids = hp.ang2pix(nside, lat, ra)

        order = np.argsort(hpids)
        hpids = hpids[order]
        values = values[order]
        pixids = np.arange(hp.nside2npix(nside))

        left = np.searchsorted(hpids, pixids)
        right = np.searchsorted(hpids, pixids, side='right')

        mapVals = np.zeros(pixids.size, dtype=dtype)

        for idx in pixids:
            mapVals[idx] = reduceFunc(values[left[idx]:right[idx]])

        if returnPixAng:
            lat, lon = hp.pix2ang(nside, pixids)
            decRet = np.pi/2. - lat
            raRet = lon
            return mapVals, raRet, decRet
        else:
            return mapVals

    tos = cat['tos']
    ra = cat['ra']
    dec = cat['dec']

    hp_map = healbin(np.radians(ra), np.radians(dec), tos,
                     nside=2*3, reduceFunc=np.sum, dtype='float')


def full_plot(name='pdbi', download=True):
    cat = get_table(name, download=download)

    print("Latest entry : " + Time(cat['date-obs'].max(), format='jyear').iso)
    # if name == 'pdbi':
    #     plot_overhead(cat)
    # animation = animate_obs(cat)
    # animation.save(name+'.gif', writer='imagemagick', fps=20, dpi=70)
    plot_sources(cat)
    plot_PI(cat)


if __name__ == "__main__":
    plt.ion()
    # full_plot('pdbi')
    # full_plot('noema')
    full_plot('pdbi+noema')
    full_plot('30m')
